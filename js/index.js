// Чому для роботи з input не рекомендується використовувати клавіатуру?

// Дані в поле для вводу input можуть попасти не лише шляхом натискання клавіш. Можно ввести дані в input голосовим повідомленням або за допомогою миші.
//  Тобто подій клавіатури може бути недостатньо, щоб корректо відстежити та обробити дані в input.
let btn = document.querySelectorAll('.btn');
  console.log(btn);
  const arr = Array.from(btn);
  console.log(arr);
  
  arr.forEach((item) => {
	document.addEventListener('keydown', function(event){
		if(event.key.toUpperCase() === item.dataset.name.toUpperCase())
		{item.classList.add('btn_active')}
	else {
	item.classList.remove('btn_active')
	 }
	
	})
	
  })
